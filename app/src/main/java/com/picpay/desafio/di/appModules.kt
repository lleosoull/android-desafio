package com.picpay.desafio.di

import androidx.room.Room
import com.picpay.desafio.database.PicPayDataBase
import com.picpay.desafio.database.dao.ContactsDAO
import com.picpay.desafio.repository.ContactsRepository
import com.picpay.desafio.ui.viewmodel.ContactsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private const val NAME_DATA_BASE = "PicPay.db"

val appModule = module {

    single<PicPayDataBase> {
        Room.databaseBuilder(
            get(),
            PicPayDataBase::class.java,
            NAME_DATA_BASE
        ).build()
    }

    single<ContactsDAO> {
        get<PicPayDataBase>().contactsDAO
    }

    single<ContactsRepository> {
        ContactsRepository(get())
    }

    viewModel {
        ContactsViewModel(get())
    }

}