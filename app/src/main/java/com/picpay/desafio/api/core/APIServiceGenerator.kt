package com.picpay.desafio.api.core

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object APIServiceGenerator {

    private const val url = "http://careers.picpay.com/tests/mobdev/"

    fun <T> createService(serviceClass: Class<T>): T {
        val builder = Retrofit.Builder()
            .baseUrl(url)
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))

        return builder.build().create(serviceClass)
    }

}