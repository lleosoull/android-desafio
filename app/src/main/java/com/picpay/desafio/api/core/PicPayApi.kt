package com.picpay.desafio.api.core

import com.picpay.desafio.interfaces.APIListener
import com.picpay.desafio.interfaces.PicPayService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class PicPayApi {

    private val errorInternet = "Por favor, verifique sua conexão com a internet e tente novamente."
    private val errorServer = "Ocorreu um erro inesperado, tente novamente"
    private val errorRandom = "Estamos trabalhando para corrigir isso, tente mais tarde"

    protected val api: PicPayService = APIServiceGenerator.createService(PicPayService::class.java)

    fun <T> Call<T>.callAsync(listener: APIListener<T>?) {
        this.enqueue(object : Callback<T> {
            override fun onFailure(call: Call<T>, t: Throwable) {
                listener?.onFailure(errorInternet)
            }

            override fun onResponse(call: Call<T>, response: Response<T>) {
                when {
                    response.isSuccessful && response.body() != null -> listener?.onSuccess(response.body()!!)
                    response.code() == 500 -> listener?.onFailure(errorServer)
                    else -> listener?.onFailure(errorRandom)
                }
            }
        })
    }
}