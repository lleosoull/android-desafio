package com.picpay.desafio.api

import com.picpay.desafio.api.core.PicPayApi
import com.picpay.desafio.interfaces.APIListener
import com.picpay.desafio.model.User

class UserApi : PicPayApi() {

    fun getUsers(listener: APIListener<List<User>>) {
        api.getUsers().callAsync(listener)
    }
}