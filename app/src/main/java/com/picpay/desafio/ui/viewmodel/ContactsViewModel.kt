package com.picpay.desafio.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.picpay.desafio.model.User
import com.picpay.desafio.repository.ContactsRepository
import com.picpay.desafio.repository.Resource

class ContactsViewModel(
    private val repository: ContactsRepository
) : ViewModel() {

    fun callContacts(): LiveData<Resource<List<User>?>> {
        return repository.callContacts()
    }

}