package com.picpay.desafio.ui.view

import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.picpay.desafio.adapter.UserListAdapter
import com.picpay.desafio.android.R
import com.picpay.desafio.ui.viewmodel.ContactsViewModel
import kotlinx.android.synthetic.main.activity_contacts.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ContactsActivity : AppCompatActivity(R.layout.activity_contacts) {

    private val adapterUser: UserListAdapter by lazy { UserListAdapter() }
    private val viewModel by viewModel<ContactsViewModel>()

    override fun onResume() {
        super.onResume()
        pbUserProgressBar.visibility = View.VISIBLE

        with(recyclerView) {
            adapter = adapterUser
            layoutManager = LinearLayoutManager(context)
        }

        viewModel.callContacts().observe(this, Observer { resources ->
            resources.dado?.let { result ->
                pbUserProgressBar.visibility = View.GONE
                adapterUser.users = result
            }
            resources.error?.let {
                pbUserProgressBar.visibility = View.GONE
                recyclerView.visibility = View.GONE

                Toast.makeText(this@ContactsActivity, it, Toast.LENGTH_SHORT).show()
            }
        })
    }
}
