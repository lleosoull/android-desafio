package com.picpay.desafio.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.picpay.desafio.database.dao.ContactsDAO
import com.picpay.desafio.model.User

@Database(entities = [User::class], version = 1)
abstract class PicPayDataBase : RoomDatabase() {

    abstract val contactsDAO: ContactsDAO

}