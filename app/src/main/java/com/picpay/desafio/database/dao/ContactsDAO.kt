package com.picpay.desafio.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.picpay.desafio.model.User

@Dao
interface ContactsDAO {

    @Query("SELECT * FROM User ORDER BY name")
    fun getAll(): LiveData<List<User>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(users: List<User>)

}