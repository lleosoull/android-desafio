package com.picpay.desafio.database.core

import android.os.AsyncTask

class PicPayTask<T>(
    private val preExecute: (() -> Unit)? = null,
    private val backGround: () -> T,
    private val postExecute: ((result: T) -> Unit)? = null
) : AsyncTask<Void, Void, T>() {

    override fun onPreExecute() {
        super.onPreExecute()
        preExecute?.let { it() }
    }

    override fun doInBackground(vararg params: Void?): T = backGround()

    override fun onPostExecute(result: T) {
        super.onPostExecute(result)
        postExecute?.let { it(result) }
    }
}