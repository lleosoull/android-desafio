package com.picpay.desafio.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.picpay.desafio.adapter.core.PicPayAdapter
import com.picpay.desafio.android.R
import com.picpay.desafio.callback.UserListDiffCallback
import com.picpay.desafio.holders.UserListItemViewHolder
import com.picpay.desafio.model.User

class UserListAdapter: PicPayAdapter<RecyclerView.ViewHolder>() {

    var users = emptyList<User>()
        set(value) {
            val result = DiffUtil.calculateDiff(
                UserListDiffCallback(
                    field,
                    value
                )
            )
            result.dispatchUpdatesTo(this)
            field = value
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListItemViewHolder {
        return UserListItemViewHolder(inflate(R.layout.list_item_user, parent))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as UserListItemViewHolder).bind(users[position])
    }

    override fun getItemCount(): Int = users.size


}