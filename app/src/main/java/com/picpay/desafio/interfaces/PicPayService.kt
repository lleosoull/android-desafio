package com.picpay.desafio.interfaces

import com.picpay.desafio.model.User
import retrofit2.Call
import retrofit2.http.GET

interface PicPayService {

    @GET("users")
    fun getUsers(): Call<List<User>>

}