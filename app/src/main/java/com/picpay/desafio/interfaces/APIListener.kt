package com.picpay.desafio.interfaces

interface APIListener<T> {
    fun onSuccess(result: T){}
    fun onFailure(response: String){}
}