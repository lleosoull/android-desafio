package com.picpay.desafio.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.picpay.desafio.api.UserApi
import com.picpay.desafio.database.core.PicPayTask
import com.picpay.desafio.database.dao.ContactsDAO
import com.picpay.desafio.interfaces.APIListener
import com.picpay.desafio.model.User

class ContactsRepository(
    private val dao: ContactsDAO
) {

    private val mediator = MediatorLiveData<Resource<List<User>?>>()

    fun callContacts(): LiveData<Resource<List<User>?>> {
        mediator.addSource(dao.getAll()) {
            mediator.value = Resource(dado = it)
        }

        val failure = MutableLiveData<Resource<List<User>?>>()
        mediator.addSource(failure) {
            val resourceCurrent = mediator.value
            val resourceNew = if (resourceCurrent != null) {
                Resource(dado = resourceCurrent.dado, error = it.error)
            } else {
                it
            }
            mediator.value = resourceNew
        }

        consultationApi { failure.value = Resource(dado = null, error = it) }

        return mediator
    }

    private fun consultationApi(
        failure: (error: String) -> Unit
    ) {
        UserApi().getUsers(object : APIListener<List<User>> {
            override fun onSuccess(result: List<User>) = save(result)

            override fun onFailure(response: String) = failure(response)
        })
    }

    private fun save(contacts: List<User>) {
        PicPayTask(
            backGround = { dao.save(contacts) }
        ).execute()
    }

}