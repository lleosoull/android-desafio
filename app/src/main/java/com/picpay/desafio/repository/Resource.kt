package com.picpay.desafio.repository

class Resource<T>(
    val dado: T,
    val error: String? = null
)