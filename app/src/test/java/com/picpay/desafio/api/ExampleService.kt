package com.picpay.desafio.api

import com.picpay.desafio.interfaces.PicPayService
import com.picpay.desafio.model.User

class ExampleService(
    private val service: PicPayService
) {

    fun example(): List<User> {
        val users = service.getUsers().execute()

        return users.body() ?: emptyList()
    }
}